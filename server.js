const express = require('express')
const bodyParser = require('body-parser')
const swaggerUi = require('swagger-ui-express')
 const swaggerDocument =require('./config/swagger.json')
const userRouter = require('./routers/user.router')
const subRouter=require('./routers/subcategorie.router')
const productRouter=require('./routers/product.router')
const orderRouter1 = require('./routers/order.router')
const categorieRouter = require('./routers/categorie.router')
const db = require('./config/database')

var app = express()
app.set('secretKey','Itgate')
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use('/api/users',userRouter)
app.use('/api/product',productRouter)
app.use('/api/subcategory',subRouter)
app.use('/api/category',categorieRouter)
app.use('/api/order',orderRouter1)


app.get("/",function(req,res){
    res.send('helloo world!')
})
// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function(req, res, next) {
    let err = new Error('Not Found');
       err.status = 404;
       next(err);
   });
   // handle errors
   app.use(function(err, req, res, next) {
    console.log(err);
    
     if(err.status === 404)
      res.status(404).json({message: "Not found"});
     else 
       res.status(500).json({message: "Something looks wrong :( !!!"});
   });app.listen(3000,function(){

    console.log('running with 3000')
})

const productController = require('../controllers/product.controller')
const express = require('express')
 const router = express.Router()

router.post('/',productController.createProduct)
router.put('/:id',productController.updateProduct)
router.delete('/:id',productController.deleteProduct)
router.get('/:page/:limit',productController.getAllProduct)
router.get('/:id',productController.getProductById)

module.exports=router


const categorieCont = require('../controllers/categorie.controller')
const express = require('express')
 const categorieRouter = express.Router()

 categorieRouter.post('/',categorieCont.createcategory)
 categorieRouter.put('/:id',categorieCont.updatecategory)
 categorieRouter.delete('/:id',categorieCont.deletecategory)
categorieRouter.get('/:page/limit',categorieCont.getAllcategory)
categorieRouter.get('/:id',categorieCont.getcategoryById)

module.exports=categorieRouter


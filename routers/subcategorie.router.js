const SubCatController = require('../controllers/subcategorie.controller')
const express = require('express')
 const router = express.Router()

router.post('/',SubCatController.createSubCat)
router.put('/:id',SubCatController.updateSubCat)
router.delete('/:id',SubCatController.deleteSubCat)
router.get('/:page/:limit',SubCatController.getAllSubCat)
router.get('/:id',SubCatController.getsubcatById)

module.exports=router


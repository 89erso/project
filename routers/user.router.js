const userController = require('../controllers/user.controller')
const express = require('express')
 const router = express.Router()
 const jwt=require('jsonwebtoken')
//public routers
router.post('/',userController.create)
router.post('/auth',userController.authenticate)
//privet routers
router.put('/:id',userController.updateUser)
router.delete('/:id',userController.deleteUser)
router.get('/:page/:limit',userController.getAll)
router.get('/:id',userController.getUserById)
router.post('/refrech',userController.refreshToken)
router.post('/logout',userController.logout)
router.post('/sendmail',userController.sendmail)
router.post('/verfier',userController.verifyuser)
module.exports=router
function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
      if (err) {
        res.json({status:"error", message: err.message, data:null});
      }else{
        // add user id to request
        req.body.userId = decoded.id;
        next();
      }
    })}

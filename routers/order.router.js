const orderCat = require('../controllers/order.controller')
const express = require('express')
 const orderRouter1 = express.Router()

orderRouter1.post('/',orderCat.createOrder)
orderRouter1.put('/:id',orderCat.updateorder)
orderRouter1.delete('/:id',orderCat.deleteorder)
orderRouter1.get('/:page/:limit',orderCat.getAllorder)
orderRouter1.get('/:id',orderCat.getorderById)

module.exports=orderRouter1


const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const OrderSchema = new Schema({
 name: {
  type: String,
  trim: true,  
  required: true,
 },
 description: {
  type: String,
  trim: true,
  required: [true,"Description is required"]
 },
 user:[{
     //get from model user
    type:Schema.Types.ObjectId,ref:'User'
}],
product:[{
    type:Schema.Types.ObjectId,ref:'Product'
}]
});

module.exports = mongoose.model('OrderSchema', OrderSchema);
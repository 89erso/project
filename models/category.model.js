const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const categorySchema = new Schema({
 name: {
  type: String,
  trim: true,  
  required: true,
 },
 description: {
  type: String,
  trim: true,
  required: [true,"Description is required"]
 },
 subcat:[{
    type:Schema.Types.ObjectId,ref:'subcategory'
 }]
});

module.exports = mongoose.model('category',categorySchema);
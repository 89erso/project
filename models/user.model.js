const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;
//Define a schema
const Schema = mongoose.Schema;
const UserSchema = new Schema({
 name: {
  type: String,
  trim: true,  
  required: true,
 },
 email: {
    type: String,
    required: [true, 'phone required'],
    //async validation
    validate: {
    validator: function (v) {
    return new Promise(function (resolve, reject) {
    //regex product code must have XXXX-XXXX-XXXX format
    //resolve(true) pass valitation
    //resolve(false) fail valitation
    resolve(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(v));
    });
    },
    message: props => `${props.value} is not a valid code format!`
    }
 },
 phone: {
    type: String,
    required: [true, 'phone required'],
    //async validation
    validate: {
    validator: function (v) {
    return new Promise(function (resolve, reject) {
    //regex product code must have XXXX-XXXX-XXXX format
    //resolve(true) pass valitation
    //resolve(false) fail valitation
    resolve(/\d{2}\d{3}\d{3}/.test(v));
    });
    },
    message: props => `${props.value} is not a valid code format!`
    }
    },
password: {
  type: String,
  trim: true,
  required: true
 },
 state: {
   type: String,
   trim: true,
   default:"0"
},
 order:[{
    type:Schema.Types.ObjectId,ref:'OrderSchema'
 }]
});
// hash user password before saving into database
UserSchema.pre('save', function(next){
this.password = bcrypt.hashSync(this.password, saltRounds);
next();
});
module.exports = mongoose.model('User', UserSchema);
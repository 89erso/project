const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const subcategorySchema = new Schema({
 name: {
  type: String,
  trim: true,  
  required: true,
 },
 description: {
  type: String,
  trim: true,
  required: true
 },
 category:{
     type:Schema.Types.ObjectId,ref:'category'
 },
 prod:[{
     type:Schema.Types.ObjectId,ref:'Product'
 }]
});

module.exports = mongoose.model('subcategory', subcategorySchema);
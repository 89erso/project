const mongoose = require('mongoose');
const { schema } = require('./user.model');
const Schema = mongoose.Schema;
const ProductShema = new Schema({
 reference: {
  type: String,
  trim: true,  
  required: [true,"ref is required"]
 },
 description: {
  type: String,
  trim: true,
  required: [true,"Description is required"]
 },
 price:{
    type: String,
  trim: true,
  required: [true,"price is required"]
 },
 subcategory:{
   type:Schema.Types.ObjectId,ref:'subcategory'
 },
 orders:[{
   type:Schema.Types.ObjectId,ref:'OrderSchema'
 }]
});

module.exports = mongoose.model('Product', ProductShema);
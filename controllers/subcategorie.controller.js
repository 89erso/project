const subcategorieModel = require('../models/subcategorie.model')

module.exports = {


    createSubCat: async function (req, res) {
        try {

            const subcategorie = await subcategorieModel.create(req.body)
            res.status(200).json({msg: 'user added', status: 200, data: subcategorie})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    getAllSubCat: async function (req, res) {
        
        
        try {

            const subcategorie = await subcategorieModel.find().select("name description")
            .limit(parseInt(req.params['limit']))
            .skip(parseInt(req.params['page'])
            *parseInt(parseInt(req.params['limit'])))
            .sort({name:'asc'})
            .populate('category').populate('prod')
            res.status(200).json({msg: ' All user ', status: 200, data: subcategorie})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },
    updateSubCat: async function (req, res) {
      
        try {
            const subcategorie = await subcategorieModel.findByIdAndUpdate({
                _id: req.params.id
            }, req.body).select('name description')
            res.status(200).json({msg: '  user Updated', status: 200, data: subcategorie})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },
    
    deleteSubCat: async function (req, res) {
       
        try {

            const subcategorie = await subcategorieModel.findByIdAndDelete({_id: req.params.id})
            res.status(200).json({msg: '  user Deleted ', status: 200, data: subcategorie})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }

    },
    getsubcatById: async function (req, res) {
        try {

            const subcategorie = await subcategorieModel.findOne({_id: req.params.id})
            res.status(200).json({msg: '  get user ', status: 200, data: subcategorie})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    login: function (req, res) {}

}

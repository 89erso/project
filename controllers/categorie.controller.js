const categoryModel = require('../models/category.model')

module.exports = {


    createcategory: async function (req, res) {
        try {

            const category = await categoryModel.create(req.body)
            res.status(200).json({msg: 'category added', status: 200, data: category})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    getAllcategory: async function (req, res) {
        
        
        try {

            const category = await categoryModel.find().select("name description")
            .limit(parseInt(req.params['limit']))
            .skip(parseInt(req.params['page'])*parseInt(req.params['limit']))
            .sort({name:'asc'}).populate('subcat')
            res.status(200).json({msg: ' All user ', status: 200, data: category})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },
    updatecategory: async function (req, res) {
      
        try {
            const category = await categoryModel.findByIdAndUpdate({
                _id: req.params.id
            }, req.body).select('name description')
            res.status(200).json({msg: '  user Updated', status: 200, data: category})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },
    
    deletecategory: async function (req, res) {
       
        try {

            const category = await categoryModel.findByIdAndDelete({_id: req.params.id})
            res.status(200).json({msg: '  user Deleted ', status: 200, data: category})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }

    },
    getcategoryById: async function (req, res) {
        try {

            const category = await categoryModel.findOne({_id: req.params.id})
            res.status(200).json({msg: '  get user ', status: 200, data: category})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    login: function (req, res) {}

}

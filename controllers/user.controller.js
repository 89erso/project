const userModel = require('../models/user.model')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
var randtoken = require('rand-token')
const nodemailer = require('nodemailer')
var refreshTokens = {}

module.exports = {


    create: async function (req, res) {
        try {

            const Users = await userModel.create(req.body)
            res.status(200).json({ msg: 'user added', status: 200, data: Users })

        } catch (err) {
            res.status(400).json({ msg: err, status: 400, data: null })

        }
    },

    getAll: async function (req, res) {


        try {

            const Users = await userModel.find().select("name email")
                .limit(parseInt(req.params['limit']))
                .skip(parseInt(req.params['page']) * parseInt(req.params['limit']))
                .sort({ name: 'asc' })
                .populate('order')
            res.status(200).json({ msg: ' All user ', status: 200, data: Users })

        } catch (err) {
            res.status(400).json({ msg: err, status: 400, data: null })

        }
    },
    updateUser: async function (req, res) {

        try {
            const user = await userModel.findOneAndUpdate({
                _id: req.params.id
            }, req.body).select('name email')
            res.status(200).json({ msg: '  user Updated', status: 200, data: user })

        } catch (err) {
            res.status(400).json({ msg: err, status: 400, data: null })

        }
    },

    deleteUser: async function (req, res) {

        try {

            const user = await userModel.findByIdAndDelete({ _id: req.params.id })
            res.status(200).json({ msg: '  user Deleted ', status: 200, data: user })

        } catch (err) {
            res.status(400).json({ msg: err, status: 400, data: null })

        }

    },
    getUserById: async function (req, res) {
        try {

            const user = await userModel.findOne({ _id: req.params.id })
            res.status(200).json({ msg: '  get user ', status: 200, data: user })

        } catch (err) {
            res.status(400).json({ msg: err, status: 400, data: null })

        }
    },
    authenticate: function (req, res, next) {
        userModel.findOne({ email: req.body.email }, function (err, userInfo) {
            if (err) {
                next(err);
            } else {
                if (bcrypt.compareSync(req.body.password, userInfo.password)) {
                    var refreshToken = randtoken.uid(256)
                    refreshTokens[refreshToken] = userInfo._id
                    const token = jwt.sign({ id: userInfo._id }, req.app.get('secretKey'), { expiresIn: '1h' });
                    res.json({ status: "success", message: "user found!!!", data: { user: userInfo, accesstoken: token, refreshToken: refreshToken } });
                } else {
                    res.json({ status: "error", message: "Invalid email/password!!!", data: null });
                }
            }
        })
    },
    refreshToken: function (req, res, next) {
        var id = req.body._id
        var refreshToken = req.body.refreshToken
        if ((refreshToken in refreshTokens) && (refreshTokens[refreshToken] == id)) {
            var user = {
                'id': id
            }
            var token = jwt.sign(user, req.app.get('secretKey'), { expiresIn: 3600 })
            res.json({ accesstoken: token })
        }
        else {
            res.send(401)
        }

    },
    logout: function (req, res, next) {
        var refreshToken = req.body.refreshToken
        if (refreshToken in refreshTokens) {
            delete refreshTokens[refreshToken]
        }
        res.status(204).json({ msg: "token expire" })
    },

    sendmail: function (req, res, next) {
        var transporter =nodemailer.createTransport( {
            host: 'smtp.mailtrap.io',
            port: 2525,
            ssl: false,
            tls: true,
            auth: {
              user: 'a995288d1e6cc1', // your Mailtrap username
              pass: '447455ba3133a6' //your Mailtrap password
            }
          })
        var mailOptions = {
            from:req.body.from,
            to:req.body.to,
            subject: req.body.subject,
            text: req.body.text,
            html:req.body.html,
            attachments:req.body.attachments
                
        }
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {

                console.log('Email sent: ' + info.response);
                res.status(200).json({msg:"email sent succ",status:200,data:mailOptions})
            }
        })

    },
    verifyuser:async function(req,res){
        try {
        const user= await userModel.findByIdAndUpdate({_id:req.params.id},req.body,{new:true})
        res.json(user)({msg:"veryfied",status:200,data:user})
        } catch (error) {
            res.json(400)({ msg:err,data:null,status:400 })
        }
    }
}

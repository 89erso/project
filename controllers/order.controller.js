const orderModel = require('../models/order.model')

module.exports = {


    createOrder: async function (req, res) {
        try {

            const order = await orderModel.create(req.body)
            res.status(200).json({msg: 'order added', status: 200, data: order})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    getAllorder: async function (req, res) {
        
        
        try {

            const order = await orderModel.find().select("name description")
            .limit(parseInt(req.params['limit']))
            .skip(parseInt(req.params['page'])*parseInt(req.params['limit']))
            .sort({name:"asc"})
            //get name of order 
            //name get only name /-_id get all whithout id
            .populate('user','name').populate('product','-_id')
            res.status(200).json({msg: ' All user ', status: 200,size:order.length,data:order})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },
    updateorder: async function (req, res) {
      
        try {
            const order = await orderModel.findByIdAndUpdate({
                _id: req.params.id
            }, req.body).select('name description')
            res.status(200).json({msg: '  user Updated', status: 200, data: order})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },
    
    deleteorder: async function (req, res) {
       
        try {

            const order = await orderModel.findByIdAndDelete({_id: req.params.id})
            res.status(200).json({msg: '  user Deleted ', status: 200, data: order})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }

    },
    getorderById: async function (req, res) {
        try {

            const order = await orderModel.findOne({_id: req.params.id})
            res.status(200).json({msg: '  get user ', status: 200, data: order})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    login: function (req, res) {}

}

const categorieModel = require('../models/product.model')

module.exports = {


    createProduct: async function (req, res) {
        try {

            const categorie = await categorieModel.create(req.body)
            res.status(200).json({msg: 'user added', status: 200, data: categorie})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },

    getAllProduct: async function (req, res) {
        
        
        try {

            const bcategorie = await categorieModel.find().select("reference description price")
            .limit(parseInt(req.params['limit']))
            .skip(parseInt(req.params['page'])*parseInt(req.params['limit']))
            .sort({name:"asc"})
            //convert id to subcategory
            .populate('subcategory')
            res.status(200).json({msg: ' All user ', status: 200, data: bcategorie})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }


    },
    updateProduct: async function (req, res) {
      
        try {
            const categorie = await categorieModel.findByIdAndUpdate({
                _id: req.params.id
            }, req.body).select('reference price description')
            res.status(200).json({msg: '  user Updated', status: 200, data: categorie})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    },
    
    deleteProduct: async function (req, res) {
       
        try {

            const categorie = await categorieModel.findByIdAndDelete({_id: req.params.id})
            res.status(200).json({msg: '  user Deleted ', status: 200, data: categorie})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }

    },
    getProductById: async function (req, res) {
        try {

            const subcategorie = await categorieModel.findOne({_id: req.params.id})
            res.status(200).json({msg: '  get user ', status: 200, data: categorie})

        } catch (err) {
            res.status(400).json({msg: err, status: 400, data: null})

        }
    }
}
